import React, { useState } from 'react';
import VizSensor from 'react-visibility-sensor';
import Head from 'next/head';
import Title from '../components/Title';
import Card from '../components/Card';
import Link from 'next/link';

const cards = [
  {
    id: 'Knight',
    className: 'move-left'
  },
  {
    id: 'Nightmare Eyes',
    className: 'move-up'
  },
  {
    id: 'Energy Vectorization',
    className: 'move-right'
  }
];

const Home: React.FC = () => {
  const [isShowingCards, setShowingCards] = useState(false);

  return (
    <div>
      <Title />
      <Head>
        <meta
          name="description"
          content="Trinity's Arena is a Living Card Game that's based on reading your opponent and responding correctly with some Spells, Swords, or by avoiding a hit."
        />
      </Head>
      <section className="hero">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Trinity&apos;s Arena</h1>
            <h2 className="subtitle">Welcome, Fighters!</h2>
            <p>
              <b>Trinity&apos;s Arena</b> is a Prototype of a Card Game
              that&apos;s based on reading your opponent and responding
              correctly with some Spells, Swords, or by avoiding a hit.
            </p>
            <p>
              It was made back in 2019, and this website is supposed to showcase
              the <Link href="/cards">Card Collection/Gallery</Link> and the{' '}
              <Link href="/guide">Guide</Link> on how to play the game.
            </p>
          </div>
        </div>
      </section>
      <div className="container">
        <VizSensor
          partialVisibility
          onChange={(isVisible) => {
            if (isVisible) setShowingCards(true);
          }}
        >
          <div className="columns is-multiline is-mobile has-text-centered">
            {cards.map((card) => (
              <div
                key={card.id}
                className={
                  'column is-one-third ' +
                  (isShowingCards ? card.className : 'before-move')
                }
              >
                <Card name={card.id} floatyness={2}></Card>
              </div>
            ))}
          </div>
        </VizSensor>
      </div>
      <br />
    </div>
  );
};

export default Home;
