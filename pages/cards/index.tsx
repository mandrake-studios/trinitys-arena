import React from 'react';
import Title from '../../components/Title';
import CardDatabase from '../../components/CardDatabase';
import ToTop from '../../components/ToTop';
import Head from 'next/head';
import { Card } from '../../common/types';
import { NextPage } from 'next';

interface CardsProps {
  data: Card[];
}

const Cards: NextPage<CardsProps> = ({ data }: CardsProps) => {
  return (
    <div>
      <ToTop />
      <Title title="Card Collection" />
      <Head>
        <meta
          name="description"
          content="A Database of all Trinity's Arena Cards | Trinity's Arena - Living Card Game"
        />
      </Head>
      <section className="section">
        <CardDatabase data={data} />
      </section>
    </div>
  );
};

export default Cards;
