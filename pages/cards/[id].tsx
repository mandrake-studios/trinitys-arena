import React from 'react';
import ErrorPage from 'next/error';
import Head from 'next/head';
import { NextPage } from 'next';

import Title from '../../components/Title';
import Card from '../../components/Card';
import { Card as CardType } from '../../common/types';

interface SingleCardProps {
  id: string;
  data: CardType[];
}

const SingleCard: NextPage<SingleCardProps> = ({
  id,
  data
}: SingleCardProps) => {
  const card = data.find((x) => x.Name === id);

  if (!card) {
    return <ErrorPage statusCode={404} />;
  }

  const text = card.Description.replace(/(<([^>]+)>)/gi, '');

  return (
    <div>
      <Head>
        <meta
          name="description"
          content={card?.Name + " | Trinity's Arena - Living Card Game"}
        />
      </Head>
      <Title title={card.Name} />
      <section className="section">
        <div className="container">
          <div className="tile is-ancestor">
            <div className="tile is-8">
              <div className="tile is-child">
                <section className="section">
                  <div className="container">
                    <h1 className="title">{card.Name}</h1>
                    <h2 className="subtitle">{card.CardArt}</h2>
                    <hr />
                    <p className="has-newline">{text}</p>
                  </div>
                </section>
              </div>
            </div>
            <div className="tile">
              <Card name={card.Name} floatyness={2}></Card>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

SingleCard.getInitialProps = (ctx) => {
  return { id: ctx.query.id as string, data: [] };
};

export default SingleCard;
