import { getServerSideSitemapLegacy } from 'next-sitemap';

import data from '../../cards.json';

const url = 'https://trinity.mandrake.games';

export const getServerSideProps = (
  ctx: import('next').GetServerSidePropsContext
): Promise<{
  // eslint-disable-next-line @typescript-eslint/ban-types
  props: {};
}> => {
  const fields: { loc: string; lastmod: string }[] = [];

  data.forEach((element) => {
    fields.push({
      loc: url + '/cards/' + element.Name,
      lastmod: new Date().toISOString()
    });
  });

  return getServerSideSitemapLegacy(ctx, fields);
};

// Default export to prevent next.js errors
// eslint-disable-next-line @typescript-eslint/no-empty-function
const preventErrors = (): void => {};
export default preventErrors;
