import ErrorPage from 'next/error';
import { Analytics } from '@vercel/analytics/react';
import { SpeedInsights } from '@vercel/speed-insights/next';
import { faAndroid } from '@fortawesome/free-brands-svg-icons/faAndroid';
import { faLinux } from '@fortawesome/free-brands-svg-icons/faLinux';
import { faWindows } from '@fortawesome/free-brands-svg-icons/faWindows';
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch';
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faUnlockAlt } from '@fortawesome/free-solid-svg-icons/faUnlockAlt';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons/faAngleDown';
import { faKey } from '@fortawesome/free-solid-svg-icons/faKey';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons/faArrowUp';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { library, config } = require('@fortawesome/fontawesome-svg-core');

import React from 'react';
import '../css/style.css';
import Nav from '../components/Nav';
import Footer from '../components/Footer';
import { Card } from '../common/types';
import data from '../cards.json';

// fix big icons
config.autoAddCss = false;

library.add(
  faSearch,
  faAndroid,
  faLinux,
  faWindows,
  faUser,
  faEnvelope,
  faUnlockAlt,
  faTimes,
  faAngleDown,
  faKey,
  faArrowUp
);

interface PageProps {
  data: Card[];
}
interface MyAppProps {
  Component: React.FC<PageProps>;
  pageProps: PageProps;
}

const MyApp: React.FC<MyAppProps> = ({ Component, pageProps }: MyAppProps) => {
  pageProps.data = data;

  if (!Component) {
    return (
      <div>
        <Nav cards={data} />
        <ErrorPage statusCode={404} />
      </div>
    );
  }

  return (
    <>
      <div className="app" id="app">
        <div className="app-div">
          <Nav cards={data} />
          <Component {...pageProps} />
        </div>
        <Footer />
      </div>
      <Analytics />
      <SpeedInsights />
    </>
  );
};

export default MyApp;
