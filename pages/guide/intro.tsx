import React from 'react';

import Guide from '../../components/Guide';
import Title from '../../components/Title';

const Classes: React.FC = () => {
  return (
    <Guide page={'intro'}>
      <Title title="Intro"></Title>
      <h1 className="title has-text-centered-mobile">Classes</h1>
      <br />
      <h3 className="subtitle is-underlined has-text-weight-bold">Neutral:</h3>
      <p>
        &ensp;These abillities don’t belong to any major type. From our research
        we believe that they might be a combination of the other types, but
        nobody knows for certain. They might even be Useless for you, try them
        and you’ll find out.
      </p>
      <br />
      <h3 className="subtitle is-underlined has-text-weight-bold">Basic:</h3>
      <p>
        &ensp;If you aren’t capable of using more advanced techniques enter at
        your own risk. But if you really want to, you at least have to Get Ready
        by mastering these maneuvers. This is the art of using your physical
        skills to Strike your opponent, with a few cunning exceptions.
      </p>
      <br />
      <h3 className="subtitle is-underlined has-text-weight-bold">
        Nightmare Eyes:
      </h3>
      <p>
        &ensp;Most common of the 5 classes. This class got it’s name from the
        very first technique that was discovered.
      </p>
      <br />
      <p>
        &ensp;They also possess an Aura (a physical extension of their body). An
        Aura consists of it’s controllers blood cells warped by intense
        emotions, intuition, and instincts. It can be molded into a vast array
        of murderous weapons and projectiles, or into any form of protection. As
        you infuse more emotions into your Aura, you become increasingly
        unstable, leading to violent outburst of Rage as you go into Emotional
        Overload.
      </p>
      <br />
      <p>
        &ensp; Nightmare Eyes the technique is actually extending Aura from your
        eyes. It envelopes the users opponent with said Aura, at this point the
        target is completely under the control of the Nightmare Eyes user.
        Because the target is enveloped by the condensed Aura he can only see
        the source of that Aura, those big red horrifying eyes, so the name
        Nightmare Eyes.
      </p>
      <br />
      <p>
        &ensp;Nightmare Eyes strength rises as stronger and more raw emotions
        are felt by the user. Although having wide range of strong emotions can
        cloud ones mind, making simple decisions in battle difficult. The class
        itself allows them to push their body to its limit, ripping and tearing
        their muscles if necessary.
      </p>
      <br />
      <h3 className="subtitle is-underlined has-text-weight-bold">
        Energy Vectorization:
      </h3>
      <p>
        &ensp;This class can manipulate the trajectory of any object in motion
        down to the smallest things that make up reality. They are able to{' '}
        Redirect any object, even weapons, launching them back at their owners,
        or even utilize the kinetic energy of atoms in motion, making them
        collide thus producing heat as a byproduct. To those less scientifically
        inclined, they can throw and burn stuff, but only if they succesfully
        calculate their magnitude, direction and location down to the minute
        details
      </p>
      <br />
      <p>
        &ensp;They can deflect straightforward projectiles, like arrows or
        bullets, since their trajectories are impossible to Miscalculate. Their
        biggest weakness in battle are melee weapons and unpredictable
        movements, since their vectors are hard to calculate. To keep a distance
        from their enemies, they Setup bomb-like traps. They are outfitted with
        a simple trigger mechanism, that can trigger on any slight interaction.
        Triggering of the trap not only unleashes its Destructive Damage , but
        it also creates very predictable vectors and movement patterns, which
        can be very dangerous when employed by Energy Vectorization.
      </p>
    </Guide>
  );
};

export default Classes;
