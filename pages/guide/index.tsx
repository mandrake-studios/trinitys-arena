import React from 'react';
import Link from 'next/link';

import Guide from '../../components/Guide';
import Card from '../../components/Card';
import Title from '../../components/Title';

const RulesPage: React.FC = () => {
  return (
    <Guide page={'welcome'}>
      <div>
        <Title title="Guide"></Title>
        <h1 className="title has-text-centered">Welcome</h1>
        <br />
        <p className="has-text-centered-desktop">
          This is where it all begins, here you will learn the proper ways of
          wielding the power within you...
          <br />
          So read carefully, or you&apos;re going to be in a lot of trouble when
          that{' '}
          <Link href={'/cards/[id]'} as={`/cards/${'Sharpest Sword'}`}>
            {`"Sharpest Sword"`}
          </Link>{' '}
          comes swinging at you, and you don&apos;t know how to use a{' '}
          <Link href={'/cards/[id]'} as={`/cards/${'Shield'}`}>
            {`"Shield"`}
          </Link>
          .
        </p>
        <div className="is-flex is-justify-content-center">
          <div className="card-width">
            <Card name="Sharpest Sword" />
          </div>
          <div className="card-width">
            <Card name="Shield" />
          </div>
        </div>
        <style jsx>{`
          .card-width {
            max-width: 375px;
          }
        `}</style>
      </div>
    </Guide>
  );
};

export default RulesPage;
