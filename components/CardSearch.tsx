import React from 'react';
import Router from 'next/router';

import { Card } from '../common/types';
import Search from './Search';

interface CardSearchProps {
  data: Card[];
  width?: string;
}

const CardSearch: React.FC<CardSearchProps> = ({
  data,
  width
}: CardSearchProps) => {
  const onSelect = (id: string) => {
    const name = data.find((card) => card.ID === id)?.Name;
    if (name) {
      return Router.push('/cards/[id]', `/cards/${name}`);
    }
    Router.push('/cards');
  };

  return (
    <Search
      data={data.map((card) => ({
        id: card.ID,
        value: card.Name,
        label: card.Name
      }))}
      onSelect={onSelect}
      width={width}
      placeholder="Search for a card..."
    />
  );
};

export default CardSearch;
