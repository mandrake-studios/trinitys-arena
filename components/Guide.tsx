import React from 'react';
import Title from './Title';
import Link from 'next/link';

interface GuideProps {
  children: React.ReactNode;
  page: string;
}

const Guide: React.FC<GuideProps> = ({ page, children }: GuideProps) => {
  const type = { [page]: true };

  return (
    <div>
      <Title />
      <div className="section">
        <div className="is-flex-tablet">
          <aside className="menu guide-nav">
            <p className="menu-label">Guide</p>
            <ul className="menu-list">
              <li>
                <Link
                  href={'/guide'}
                  className={type.welcome ? 'is-active' : ''}
                >
                  Welcome
                </Link>
              </li>
              <li>
                <Link
                  href={'/guide/intro'}
                  className={type.intro ? 'is-active' : ''}
                >
                  Intro
                </Link>
              </li>
              <li>
                <a href="/rulebook.pdf" target="_blank">
                  Rules [PDF]
                </a>
              </li>
            </ul>
            <style jsx>{`
              .guide-nav {
                padding-right: 15px;
                padding-bottom: 30px;
                min-width: 150pt;
              }
            `}</style>
          </aside>
          <div className="guide-body">{children}</div>
        </div>
      </div>
      <style>{`
        .guide-body {
          max-width: 1400px;
          min-width: 70%;
          margin-right: auto;
          margin-left: auto;
        }
      `}</style>
    </div>
  );
};

export default Guide;
