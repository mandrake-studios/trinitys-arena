import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface SearchProps {
  data: {
    id: string;
    value: string;
    label: string;
  }[];
  width?: string;
  placeholder?: string;
  onSelect: (id: string) => void;
}

const Search: React.FC<SearchProps> = ({
  data,
  placeholder,
  onSelect,
  width
}: SearchProps) => {
  const [foundItems, setFound] = useState([]);
  const [selected, setSelected] = useState(-1);
  const [searchBar, setSearchBar] = useState<HTMLInputElement>();

  const reset = () => {
    searchBar.value = '';
    setSelected(-1);
    setFound([]);
  };

  const onChange = () => {
    const searchVal = searchBar.value.toLowerCase();

    if (searchVal === '') {
      return setFound([]);
    }

    setFound(
      data.filter((item) => item.value.toLowerCase().includes(searchVal))
    );
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const index = selected > -1 ? selected : 0;

    const id = foundItems[index]?.id;
    reset();
    return onSelect(id);
  };

  const handleKeyDown = (e) => {
    const cursor = selected;
    if (e.keyCode === 38 && cursor > -1) {
      setSelected(cursor - 1);
    } else if (e.keyCode === 40 && cursor < foundItems.length - 1) {
      setSelected(cursor + 1);
    } else if (e.keyCode === 27) {
      reset();
    }
  };

  return (
    <>
      <div>
        <form onSubmit={handleSubmit} className="bar-width">
          <div className="field is-grouped">
            <div className="control is-expanded has-icons-left">
              <input
                className="bar-width input"
                ref={(c) => {
                  setSearchBar(c);
                }}
                type="text"
                placeholder={placeholder}
                onChange={onChange}
                onKeyDown={handleKeyDown}
                onBlur={reset}
              />
              <span className="icon is-left">
                <FontAwesomeIcon icon="search" />
              </span>
            </div>
          </div>
        </form>
        {foundItems.length > 0 && (
          <div className="menu-item-container dropdown-content bar-width">
            {foundItems.slice(0, 7).map((item, index) => {
              return (
                <div
                  key={item.id}
                  className={'menu-item dropdown-item '.concat(
                    index === selected ? 'menu-item-active' : ''
                  )}
                  onMouseEnter={() => {
                    setSelected(index);
                  }}
                  onMouseLeave={() => {
                    setSelected(-1);
                  }}
                  onMouseDown={handleSubmit}
                >
                  {item.label}
                </div>
              );
            })}
          </div>
        )}
      </div>
      <style global jsx>{`
        .menu-item {
          width: 100%;
          margin: 0px 0px;
          float: right;
          cursor: pointer;
        }
        .menu-item-container {
          z-index: 1;
          position: absolute;
        }
        .menu-item-active {
          background-color: #f5f5f5;
        }
        .bar-width {
          width: ${width ? width + 'px' : 'auto'};
        }
      `}</style>
    </>
  );
};

export default Search;
