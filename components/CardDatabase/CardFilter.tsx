import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Filter } from './types';

interface CardFilterProps {
  displayText: string;
  chosen: Filter;
  all: Filter[];
  collapse: boolean;
  handleToggle: () => void;
  Change: (filter: Filter) => void;
}
const CardFilter: React.FC<CardFilterProps> = ({
  chosen,
  displayText,
  collapse,
  handleToggle,
  Change,
  all
}: CardFilterProps) => {
  return (
    <>
      <div className="filter-container">
        <div className={'dropdown' + (collapse ? '' : ' is-active')}>
          <div className="dropdown-trigger">
            <button
              className="filter button is-size-7-mobile"
              aria-haspopup={true}
              aria-controls="dropdown-menu"
              onClick={handleToggle}
              id="filter"
            >
              <span id="filter">{`${displayText} : ${chosen.display}`}</span>
              <span className="icon is-small" id="filter">
                <FontAwesomeIcon id="filter" icon="angle-down" />
              </span>
            </button>
          </div>
          <div className="dropdown-menu" id="dropdown-menu" role="menu">
            <div className="dropdown-content">
              {all.map((ele, i) => (
                <a
                  key={i}
                  className={
                    'dropdown-item' + (chosen === ele ? ' is-active' : '')
                  }
                  onClick={() => Change(ele)}
                >
                  {ele.display}
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .filter-container {
          padding-right: 10pt;
          padding-bottom: 10pt;
        }
        .filter {
          justify-content: space-between;
        }
      `}</style>
    </>
  );
};

export default CardFilter;
