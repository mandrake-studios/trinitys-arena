import React, { useEffect, useState } from 'react';
import { Card as CardType } from '../../common/types';
import { useRouter } from 'next/router';
import { Filter } from './types';
import DatabaseCard from './DatabaseCard';
import CardSearch from './CardSearch';
import CardFilter from './CardFilter';

const types: Filter[] = [
  {
    display: 'None',
    filter: ''
  },
  {
    display: 'Mark',
    filter: 'M'
  },
  {
    display: 'Trap',
    filter: 'T'
  },
  {
    display: 'Equipment',
    filter: 'E'
  },
  {
    display: 'Board',
    filter: 'B'
  }
];

const classes: Filter[] = [
  {
    display: 'None',
    filter: ''
  },
  {
    display: 'Neutral',
    filter: 'Neutral'
  },
  {
    display: 'Knight',
    filter: 'Knight'
  },
  {
    display: 'Nightmare Eyes',
    filter: 'Red'
  },
  {
    display: 'Energy Vectorization',
    filter: 'Yellow'
  }
];

interface CardDatabaseProps {
  data: CardType[];
}

const CardDatabase: React.FC<CardDatabaseProps> = ({
  data
}: CardDatabaseProps) => {
  const router = useRouter();
  const [display, setDisplay] = useState(data);
  const [collapseType, setCollapseType] = useState(true);
  const [collapseClass, setCollapseClass] = useState(true);

  const [filter, setFilter] = useState(
    router.query.search ? (router.query.search as string) : ''
  );
  const [chosenClass, setClass] = useState(
    (() => {
      const routerClass = classes.find(
        (x) => x.filter === (router.query.class as string)
      );

      return routerClass ? routerClass : classes[0];
    })()
  );
  const [chosenType, setType] = useState(
    (() => {
      const routerType = types.find(
        (x) => x.filter === (router.query.type as string)
      );

      return routerType ? routerType : types[0];
    })()
  );

  useEffect(() => {
    if (filter === '') {
      setDisplay(data);
    } else {
      const newDisplay: CardType[] = [];
      const searchVal = filter.toLowerCase();

      if (data.length > 0) {
        data.forEach((card) => {
          if (
            card.Name.toLowerCase().includes(searchVal) ||
            card.Description.toLowerCase().includes(searchVal)
          ) {
            newDisplay.push(card);
          }
        });
        setDisplay(newDisplay);
      }
    }
  }, [filter, data]);

  const handleToggleType = () => {
    setCollapseType(!collapseType);
    setCollapseClass(true);
  };

  const handleToggleClass = () => {
    setCollapseType(true);
    setCollapseClass(!collapseClass);
  };

  useEffect(() => {
    const EventListener = (event) => {
      const target = event.target;
      if (target.id === 'filter') {
        return; // filter button was clicked, ignore onClick
      }
      setCollapseType(true);
      setCollapseClass(true);
    };

    window.addEventListener('click', EventListener);

    return () => window.removeEventListener('click', EventListener);
  }, []);

  useEffect(() => {
    const query: {
      search?: string;
      type?: string;
      class?: string;
    } = {};

    if (filter !== '') {
      query.search = filter;
    }

    if (chosenType.filter !== '') {
      query.type = chosenType.filter;
    }

    if (chosenClass.filter !== '') {
      query.class = chosenClass.filter;
    }

    router.replace(
      {
        pathname: router.pathname,
        query: query
      },
      undefined,
      {
        shallow: true
      }
    );
  }, [chosenClass, chosenType, filter]);

  const ChangeType = (filter: Filter) => {
    setType(filter);
    setCollapseType(true);
  };

  const ChangeClass = (filter: Filter) => {
    setClass(filter);
    setCollapseClass(true);
  };

  const handleFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFilter(event.target.value);
  };

  return (
    <div className="container">
      <div className="card" style={{ padding: '1rem', marginBottom: '0.8rem' }}>
        <CardSearch handleFilterChange={handleFilterChange} filter={filter} />
        <div className="is-block-tablet is-flex-desktop">
          <CardFilter
            all={classes}
            Change={ChangeClass}
            handleToggle={handleToggleClass}
            collapse={collapseClass}
            chosen={chosenClass}
            displayText={'Class'}
          />
          <CardFilter
            all={types}
            Change={ChangeType}
            handleToggle={handleToggleType}
            collapse={collapseType}
            chosen={chosenType}
            displayText={'Type'}
          />
        </div>
      </div>
      <DatabaseCard
        display={display}
        chosenClass={chosenClass}
        chosenType={chosenType}
      />
    </div>
  );
};

export default CardDatabase;
