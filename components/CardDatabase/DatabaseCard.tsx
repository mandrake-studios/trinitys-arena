import React from 'react';

import { Card as CardType } from '../../common/types';
import Card from '../../components/Card';
import { Filter } from './types';

interface DatabaseCardProps {
  display: CardType[];
  chosenType: Filter;
  chosenClass: Filter;
}
const DatabaseCard: React.FC<DatabaseCardProps> = ({
  display,
  chosenType,
  chosenClass
}: DatabaseCardProps) => {
  return (
    <div style={{ paddingLeft: '0.65rem', paddingRight: '0.65rem' }}>
      <div className="columns is-multiline is-mobile">
        {display
          .filter(
            (x) =>
              x.TYPE.includes(chosenType.filter) &&
              x.CardArt.includes(chosenClass.filter)
          )
          .map((card) => (
            <div
              className={`column is-full-mobile is-4-tablet-desktop is-one-fifth-widescreen is-one-fifth-fullhd`}
              key={card.ID}
              style={{ padding: 0 }}
            >
              <div className="card" style={{ margin: '0.1rem' }}>
                <Card name={card.Name}></Card>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default DatabaseCard;
